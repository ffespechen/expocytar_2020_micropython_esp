# D1 -> GPIO5 ROJO
# D2 -> GPIO4 VERDE
# D5 -> GPI14 AZUL

import machine, time

pwm_R = machine.PWM(machine.Pin(5))
pwm_G = machine.PWM(machine.Pin(4))
pwm_B = machine.PWM(machine.Pin(14))

print('\n--------- RGB progresivo ROJO ---------')
for d in range(0, 1024, 10):
    print('Duty : ', d, end=' ')
    pwm_R.duty(d)
    time.sleep(0.05)
    
pwm_R.duty(0)

print('\n--------- RGB progresivo VERDE ---------')

for d in range(0, 1024, 10):
    print('Duty : ', d, end=' ')
    pwm_G.duty(d)
    time.sleep(0.05)

pwm_G.duty(0)

print('\n--------- RGB progresivo AZUL ---------')

for d in range(0, 1024, 10):
    print('Duty : ', d, end=' ')
    pwm_B.duty(d)
    time.sleep(0.05)
    
pwm_B.duty(0)