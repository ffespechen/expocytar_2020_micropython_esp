# GPIO4 -> D2
# GPIO5 -> D1

import machine, time

# Defino como salida el GPIO4
led_rojo = machine.Pin(4, machine.Pin.OUT)
led_rojo.on()

# Defino como salida el GPIO5
led_verde = machine.Pin(5, machine.Pin.OUT)
led_verde.on()

time.sleep(3)

led_rojo.off()
led_verde.off()

print('Finalizado primera parte del programa')
print('Comenzando SEGUNDA PARTE - Bucle')

for i in range(20):
    print('Loop número... ', i)
    led_rojo.on()
    led_verde.off()
    time.sleep(0.5)
    led_rojo.off()
    led_verde.on()
    time.sleep(0.5)