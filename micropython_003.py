# D2 -> GPIO4
# rojo   = VCC
# naraja = señal
# marrón = GND

import machine, time

pwm_motor = machine.PWM(machine.Pin(14), freq=50)

pwm_motor.duty(0)

for d in range(30, 122, 1):
    print(d)
    pwm_motor.duty(d)
    time.sleep(0.5)

for d in range(122, 30, -1):
    pwm_motor.duty(d)
    time.sleep(0.5)
    print(d)
